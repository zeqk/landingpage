import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

import { QuoteService } from './quote.service';
import { ResumeService } from './resume.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  quote: string;
  isLoading: boolean;
  resume: any = null;
  content: string;
  language = 'json';
  interpolate = {
    language: 'language interpolated'
  };

  constructor(
    private quoteService: QuoteService,
    private resumeService: ResumeService,
    private translateService: TranslateService
  ) {
    this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.getResume(event.lang);
    });
  }

  ngOnInit() {
    this.isLoading = true;

    this.getResume('en-US');
  }

  getResume(language: string) {
    this.resumeService
      .getResume(language)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe((resume: any) => {
        this.resume = resume;
        this.content = JSON.stringify(resume, null, 2);
      });
  }
}
