import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class ResumeService {
  constructor(private httpClient: HttpClient) {}

  getResume(language: string): Observable<any> {
    // this.httpClient.disableApiPrefix();
    return this.httpClient
      .cache()
      .get(`data/${language}/resume.json`)
      .pipe(
        // map((body: any) => {
        //   debugger;
        //   return body.value;
        // }),
        catchError(() => of('Error, could not load joke :-('))
      );
  }
}
